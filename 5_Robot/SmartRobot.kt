open class smartRobot(x: Int, y: Int, direction: Direction):
    Robot(x,y, direction){
    fun moveRobot(toX: Int, toY: Int) {
        if(toX-x < 0){turnRight()}
        else if(toX-x > 0){turnLeft()}
        else if(toY-y >0){turnLeft()}
        else if(toY-y<0){turnRight()}
        x = toX
        y = toY
    }
    fun turnRight(){
        direction = when (direction){
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.LEFT -> Direction.UP
            Direction.RIGHT -> Direction.DOWN
        }
    }
    fun turnLeft() {
        direction = when (direction) {
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.LEFT -> Direction.DOWN
            Direction.RIGHT -> Direction.UP
        }
    }
}
