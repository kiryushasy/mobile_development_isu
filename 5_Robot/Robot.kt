open class Robot(var x: Int, var y: Int, open var direction: Direction) {
    fun stepForward(){
        when(direction){
            Direction.UP -> y++
            Direction.LEFT -> x--
            Direction.DOWN -> y++
            Direction.RIGHT -> x++
        }
    }


    override fun toString(): String{
        return "(${x}, ${y}), looks ${direction}"
    }
}
