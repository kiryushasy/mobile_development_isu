import java.lang.Math.abs
import java.lang.Math.max
fun main(){
        var (k, x, y) = readLine()!!.split(' ').map(String::toInt)
    max(k,x,y)

}
fun max (k:Int, x:Int, y:Int) : Int{
    if(abs(x - y) >= 2 && max(x, y) >= k){
        return(0)
    }
    else if(x == y){
        if(x >= k){
            print(2)
        }
        else if(k - x >= 1) {
            print(k - x)
        }
        else print(2)
    }
    else if(k > max(x, y)){
        if(x > y){
            print(k - x)
        }
        else print(k - y)
    }
    else return(1)
    return 0

}