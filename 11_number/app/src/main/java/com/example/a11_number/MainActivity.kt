package com.example.a11_number
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var min_: EditText
    private lateinit var max_: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        min_ = findViewById(R.id.minId)
        max_ = findViewById(R.id.maxId)
    }

    fun click(view: View){
        val minNum =min_.text.toString().toInt()
        val maxNum = max_.text.toString().toInt()

        if (minNum >= maxNum){
            Toast.makeText(this, "Минимум должен быть мньше Максимума", Toast.LENGTH_LONG).show()
        }
        else{
            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("min", minNum)
            intent.putExtra("max", maxNum)
            startActivity(intent)
        }

    }
}
