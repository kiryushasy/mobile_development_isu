package com.example.a11_number

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    var minNum = 0
    var maxNum = 0
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        minNum = intent.getIntExtra("min", 0)
        maxNum = intent.getIntExtra("max", 0)
        textView = findViewById(R.id.textView)
        textView.text = ((maxNum + minNum)/2).toString()
    }
    fun click2(view: View){
        if (maxNum - minNum <=1){
            textView.text = "Ваше число $minNum"
        }
        else{
            if(view.id == R.id.btn1){
                maxNum = (maxNum+minNum)/2
            }
            else if(view.id == R.id.btn2){
                minNum = (maxNum+minNum)/2
            }
            textView.text = "${((maxNum + minNum) / 2)}"
        }


    }
}
