import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.PrintWriter
import java.nio.charset.Charset
import java.util.*

data class Message(val address: String?, val topic: String?, val sender:String?, val mes: String?) {
    var out= File("index.html")
    //fun toHTML(): String = "$address ${topic}"
    fun toHTML():String{
        val template =
            "<!DOCKTYPE html> <html lang = 'en'>"+
                    "<head>" +
                    "<style type='text/css'>"+
                    "table {"+
                    "border-collapse: collapse;" +
                    "text-align: center;" +
                    "}" +

                    "td:first-child {" +
                    "background: #FF0000;" +
                    "color: white;" +
                    "padding: 10px 20px;" +
                    "}" +

                    "td {" +
                    "border-style: solid;" +
                    "border-width: 0 1px 1px 0;" +
                    "border-color: white;" +
                    "}" +

                    "td {" +
                    "background: #0000FF;" +
                    "}" +
                    "td:first-child {" +
                    "text-align: left;" +
                    "}" +
                    "</style>"+
                    "</head>"+
                    "<table> " + "\n" +
                    if(address != null){
                        address?.let { "<tr> \n \t <td>address</td><td>$it</td>\n</tr> \n" }
                    } else {
                        address?.let{it}?:"<tr>\n \t<td>address</td><td></td>\n</tr> \n"
                    }+
                    if(topic != null){
                        topic?.let { "<tr>\n \t<td>topic</td><td>$it</td>\n</tr> \n" }
                    } else {
                        topic?.let{it}?:"<tr>\n \t<td>topic</td><td></td>\n</tr> \n"
                    }+
                    if(sender != null){
                        sender?.let { "<tr>\n \t<td>sender</td><td>$it</td>\n</tr> \n" }
                    } else {
                        sender?.let{it}?:"<tr>\n \t<td>sender</td><td></td>\n</tr> \n"
                    }+
                    if(mes != null){
                        mes?.let { "<tr>\n \t<td>mes</td><td>$it</td>\n</tr> \n" }
                    } else {
                        mes?.let{it}?:"<tr>\n \t<td>mes</td><td></td>\n</tr> \n"
                    }+
                    "</table>"
        PrintWriter(out, Charsets.UTF_8).use{it.println(template)}
        return template
    }
}

fun main() {
    val m = Message("check@email.ru", null, "sender@mail.ru", null)
    println(m.toHTML())
}
