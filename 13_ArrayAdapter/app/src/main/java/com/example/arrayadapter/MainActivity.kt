package com.example.arrayadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import java.util.Random

class MainActivity : AppCompatActivity() {
    private lateinit var names: ArrayList<String>
    private lateinit var lastNames: ArrayList<String>
    private lateinit var addButton: Button
    private lateinit var rand: Random


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var listIt = findViewById<ListView>(R.id.listId)
        names = resources.getStringArray(R.array.Names).toCollection(ArrayList())
        lastNames = resources.getStringArray(R.array.Surnames).toCollection(ArrayList())
        addButton = findViewById(R.id.addBtn)
        rand = Random()
        var fullName: MutableList<String> = ArrayList()
        var adapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_list_item_1, fullName)
        listIt.setAdapter(adapter)
        addButton.setOnClickListener{
            var idx = rand.nextInt(names.size)
            var idx2 = rand.nextInt(lastNames.size)
            var full_name = names[idx].toString() + " " + lastNames[idx2].toString()
            fullName.add("${full_name}")
            adapter.notifyDataSetChanged()
        }
    }

}