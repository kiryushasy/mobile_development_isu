import kotlin.math.pow
import kotlin.math.PI
import kotlin.math.sqrt
interface Movable {

    fun move(dx: Int, dy: Int)
}
interface Transforming {
    fun resize(zoom: Int)

    fun rotate(direction: RotateDirection, centerX: Int, centerY: Int)
}


abstract class Figure(val id: Int) {
    abstract fun area(): Float
}

class Circle(var radius: Float, var x: Int, var y: Int) : Transforming, Figure(0) {
    override fun area(): Float {
        return PI.toFloat() * radius.pow(4);
    }

    override fun resize(zoom: Int) {
        radius *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return
        val (signX, signY) = if (direction == RotateDirection.COUNTER_CLOCK_WISE) Pair(-1, 1) else Pair(1, -1)

        x = (signX * (y - centerY) + centerX).also {
            y = signY * (x - centerX) + centerY
        }
    }

    override fun toString(): String {
        return "Круг ($x, $y) [r: $radius]"
    }
}
enum class RotateDirection {
    CLOCK_WISE, COUNTER_CLOCK_WISE
}

class Rect(var x: Int, var y: Int, var width: Float, var height: Float) : Movable, Transforming, Figure(0) {

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun area(): Float {
        return width * height
    }

    override fun resize(zoom: Int) {
        width *= zoom
        height *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return

        val (signX, signY) = if (direction == RotateDirection.COUNTER_CLOCK_WISE) Pair(-1, 1) else Pair(1, -1)

        x = (signX * (y - centerY) + centerX).also {
            y = signY * (x - centerX) + centerY
        }

        width = height.also {
            height = width
        }
    }

    override fun toString(): String {
        return "Прямоугольник ($x, $y) [w: $width, h: $height]"
    }
}

class Square(var x: Int, var y: Int, var width: Float) : Transforming, Figure(0) {
    override fun area(): Float {
        return width.pow(2)
    }

    override fun resize(zoom: Int) {
        width *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return
        val (signX, signY) = if (direction == RotateDirection.COUNTER_CLOCK_WISE) Pair(-1, 1) else Pair(1, -1)

        x = (signX * (y - centerY) + centerX).also {
            y = signY * (x - centerX) + centerY
        }
    }

    override fun toString(): String {
        return "Квадрат ($x, $y) [w: $width]"
    }
}





fun checkMethods(
    figureName: String,
    obj: Transforming,
    resize: Int,
    rDirection: RotateDirection = RotateDirection.CLOCK_WISE,
    CenterX: Int = 0,
    CenterY: Int = 0
) {
    println("$figureName перед resize(2): $obj")
    obj.resize(2)
    println("$figureName после resize(2): $obj")

    println("-".repeat(150))

    println("$figureName перед rotate(Figures.Enums.RotateDirection.Clockwise, 0, 0): $obj")
    obj.rotate(rDirection, CenterX, CenterY)
    println("$figureName после rotate(Figures.Enums.RotateDirection.Clockwise, 0, 0): $obj")

    println("*".repeat(150))
}

fun main() {
    val tr: Transforming = Rect(0, 3, 1f, 2f)
    checkMethods(figureName = "Прямоугольник", obj = tr, resize = 2)

    val tc: Transforming = Circle(3f, 0, 0)
    checkMethods(
        figureName = "Круг",
        obj = tc,
        resize = 2,
        rDirection = RotateDirection.COUNTER_CLOCK_WISE,
        CenterY = 3
    )

    val ts: Transforming = Square(0, 3, 5f)
    checkMethods(figureName = "Квадрат", obj = ts, resize = 2, CenterX = 3)
}
