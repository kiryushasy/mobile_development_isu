# Создание классов с интерфейсом Transforming
Используя как основу <a href="https://github.com/ipetrushin/Figures">проект</a>, дополните классы `Rect`, `Circle`, `Square` необходимой функциональностью:
<ul>
    <li>реализуйте интерфейс Transforming</li>
    <li>осуществите наследование от Figure</li>
<ul>
В классе Main проверьте работоспособность методов: поворот, перемещение и масштабирование фигур

## Демонстрация работы:
![Иллюстрация](https://gitlab.com/kiryushasy/mobile_development_isu/-/blob/main/6_class_Transforming/Screenshot_9.png)
