package com.example.adapt

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    private lateinit var button: Button

    var cnt = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main)
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main)
        }
    }

    fun onChangePictureClick(view: View){
        val iv = findViewById<ImageView>(R.id.picture)
        val button = findViewById<Button>(R.id.btn)
        cnt+=1
        if(cnt == 1){
            iv.setImageResource(R.drawable.tf2)
        }
        if(cnt == 2){
            iv.setImageResource(R.drawable.tf3)
        }
        if(cnt == 3){
            cnt = 0
            iv.setImageResource(R.drawable.tf1)
        }

//        button.setOnClickListener{
////            iv.setImageResource(img[cnt])
//            cnt+=1
//            iv.setImageResource(img[cnt])
//            if (cnt>3){
//                cnt = 0
//                iv.setImageResource(img[cnt])
//            }
//        }

    }
}